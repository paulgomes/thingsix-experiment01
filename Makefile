# Makefile example came from here:
# http://hiltmon.com/blog/2013/07/03/a-simple-c-plus-plus-project-structure/
#
CC := msp430-gcc  #gcc or clang-3.5
SRCDIR := src
BUILDDIR := build
TARGETDIR := bin
TARGET := thingsix
DEVICE = generic #cc2650 msp430g2553 generic

SRCEXT := c
SOURCES := src/main.c $(shell find $(SRCDIR)/drivers/$(DEVICE) -type f -name *.$(SRCEXT))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))
CFLAGS := -D__$(DEVICE) -g # -Wall
LIB := -L$(SRCDIR)/drivers/msp430g2553/ldscripts
INC := -I$(SRCDIR)/drivers #-I$(SRCDIR)/drivers/***

$(TARGET): $(OBJECTS)
	@echo " Linking..."
	@echo " $(CC) $^ -o $(TARGETDIR)/$(TARGET) -mmcu=$(DEVICE) $(LIB)"; $(CC) $^ -o $(TARGETDIR)/$(TARGET) $(LIB)

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(@D)
	@mkdir -p $(TARGETDIR)
	@echo " $(CC) $(CFLAGS) $(INC) -c -o $@ $<"; $(CC) $(CFLAGS) $(INC) -c -o $@ $<

clean:
	@echo " Cleaning..."; 
	@echo " $(RM) -r $(BUILDDIR) $(TARGETDIR)"; $(RM) -r $(BUILDDIR) $(TARGETDIR)

.PHONY: clean