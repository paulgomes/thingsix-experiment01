# README #
Experiment with alternate thingsix project structure.
This approach will compile for the specified target device.
Look at $/src/drivers/thingsix.h
It uses preprocessor directives to determine which driver to compile.

### Build
To build for a generic device:

```sh
$ make
```
Or build for the specified device:
```sh
$ make DEVICE=cc2650
```
```sh
$ make DEVICE=msp430g2553
```

### Run
```sh
$ bin/thingsix
```

