#ifndef __THINGSIX
#define __THINGSIX

#if defined (__msp430g2553)
#include "msp430g2553/msp430g2553-driver.h"

#elif defined (__cc2650)
#include "cc2650/cc2650-driver.h"

#elif defined (__generic)
#include "generic/generic-driver.h"

#else
#error "Failed to match driver include file"
#endif

#endif /* __THINGSIX */
