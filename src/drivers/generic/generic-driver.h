#ifndef __GENERIC_DRIVER
#define __GENERIC_DRIVER

#include <stdbool.h>

void device_init();
void led(unsigned pin, bool value);

#endif /* __GENERIC_DRIVER */
