#include "generic-driver.h"
#include <stdio.h>

void device_init() {
  printf("generic device_init\n");
}

void led(unsigned pin, bool value)
{
  printf("generic device driver: Setting pin %u to %u.\n", pin, (unsigned)value);
}
