#ifndef __MSP430G2553_DRIVER
#define __MSP430G2553_DRIVER

#include <stdbool.h>

void device_init();
void led(unsigned pin, bool value);

#endif /* __MSP430G2553_DRIVER */
