#include "msp430g2553-driver.h"
#include "msp430g2553.h"

void device_init() {
  /* Hold the watchdog */
  WDTCTL = WDTPW + WDTHOLD;
}

void led(unsigned pin, bool value) {
  // TODO: Use pin param to choose P1DIR and P1OUT

  /* Set P1.0 direction to output */
  P1DIR |= 0x01;

  /* Set P1.0 output 1=high, 0=low */
  P1OUT |= value;
}
