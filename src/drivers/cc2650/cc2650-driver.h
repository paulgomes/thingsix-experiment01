#ifndef __CC2650_DRIVER
#define __CC2650_DRIVER

#include <stdbool.h>

void device_init();
void led(unsigned pin, bool value);

#endif /* __CC2650_DRIVER */
