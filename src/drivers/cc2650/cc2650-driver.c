#include "cc2650-driver.h"
#include <stdio.h>

void device_init()
{
  printf("cc2650 device_init\n");
}

void led(unsigned pin, bool value)
{
  printf("cc2650 device driver: Setting pin %u to %u.\n", pin, (unsigned)value);
}
